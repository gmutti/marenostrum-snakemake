#!/bin/bash

# Set the directory path
config_dir="$HOME/.config/snakemake"
mn_dir="$config_dir/mn"

# Check if ~/.config/snakemake/ exists, create it if not
if [ ! -d "$config_dir" ]; then
  echo "Creating directory: $config_dir"
  mkdir -p "$config_dir"
fi

# Check if ~/.config/snakemake/mn exists, clone the repository if not
if [ ! -d "$mn_dir" ]; then
  echo "Cloning repository into: $mn_dir"
  git clone https://gitlab.bsc.es/gmutti/marenostrum-snakemake "$mn_dir"
else
  echo "Directory $mn_dir already exists. Skipping cloning."
fi

echo "Installation completed."
