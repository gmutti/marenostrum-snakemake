# Snakemake profile for BSC's MareNostrum cluster

This repo is adapted from [Vince Buffalo repo for Savio cluster](https://github.com/vsbuffalo/savio-snakemake).

This is a [Snakemake
profile](https://snakemake.readthedocs.io/en/stable/getting_started/migration.html#profiles)
for BSC MareNostrum5 cluster. Snakemake versions greater than 8 rely on
these profiles to dispatch Snakemake rules as jobs onto clusters via their
schedulers (e.g. [Slurm](https://slurm.schedmd.com/documentation.html)).
Snakemake recently deprecated cluster configuration files; this profile
will migrate that functionality. Thanks to [Silas
Tittes](https://twitter.com/SilasTittes) for sharing his profile that
worked for the [UO Cluster talapas](https://racs.uoregon.edu/talapas), from
which this is based upon.

## Warning

Given that MN is used by a lot of people I reccomend to specify which rules 
should not be sent to mn using the [`localrule` directive](https://snakemake.readthedocs.io/en/stable/snakefiles/rules.html). 

Further, if your pipeline's DAG is huge please consider [grouping jobs](https://snakemake.readthedocs.io/en/stable/executing/grouping.html).

## Installation

You can install with:

```
$ curl https://gitlab.bsc.es/gmutti/marenostrum-snakemake/-/raw/main/install.sh | bash
```

This will run the `install.sh` script, which will create the
`~/.config/snakemake/` directory if it does not exist, and then clone this
repository into `~/.config/snakemake/mn/`. This will place the workflow
profile at `~/.config/snakemake/mn/config.yaml`. 

## Configuration

By defauly you will no need to configure the file at `~/.config/snakemake/mn/config.yaml`:

1. The `slurm_partition` is `gp_bscls` but you can change to any partition [mn
   partition](https://www.bsc.es/supportkc/docs/MareNostrum5/overview)
   you want to use. You can also do this at the rule-level (see below) in the `resources` block.
   This could be useful if only one rule in your pipeline requires high-mem for example.

2. We all share the same `slurm_account` name so no need to change it

3. `mem_mb`: do not touch it as MN does not allow to set memory

## Usage

Then, once configured, this Snakemake profile can be used in workflows by
running `Snakemake`. In MN it is best to send the snakemake command in a `nohup` session

```bash
$ nohup snakemake --workflow-profile ~/.config/snakemake/mn all 
```

Note that this job will use the **default resources** and only *two* jobs as specified in the `config.yaml`. This is 
intentional, since the user should override the number of jobs on the command line with 

```bash
$ snakemake -j 24 --workflow-profile ~/.config/snakemake/mn all 
```

or, set the resources in the Snakemake rule itself, e.g. 

```python
rule a:
  input: "some_infile.txt"
  output: "some_outfile.txt"
  resources:
    mem_mb_per_cpu=1800,
    cpus_per_task=40,
    runtime=60,
    slurm_extra="'--qos=acc_bscls'"
  shell:
    """
    # some command
    """
```

This is more reproducible, and helps future researchers see what resources
are needed for larger jobs. If the defaults in `config.yaml` are too far 
different from your needs, you can alter the local `config.yaml`.

## Example

As a minimal reproducible example, this repository includes a test `Snakefile`
in `mn_test/`. I will use [mamba](https://github.com/mamba-org/mamba) to
install the packages (which should be available on mn). Go to the test
directory, create a Python virtual environment from the `requirements.txt`, and
activate it:

```bash
$ cd ~/.config/snakemake/mn/mn_test/ 
$ mamba create -n snakemake_env -c conda-forge -c bioconda --file requirements.txt
$ mamba activate snakemake_env
```

The `Snakefile` (shown below) will simply spawn jobs the node's hostname
to a results directory:

```Snakefile
rule test_rule:
  output: "results/{letter}.txt"
  shell:
     """
     hostname > {output}
     """

rule all:
  input: expand("results/{letter}.txt", letter=["A", "B", "C", "D", "E"])
```

To run this with four jobs, use

```
$ snakemake --workflow-profile ~/.config/snakemake/mn all --jobs 4
```

Then, you can use `squeue` to see that the jobs
launched. Again, for large jobs, I recommend spawning a new interactive session
with `nohup`, since sometimes large, complicated DAGs can be resource-intensive
to create and also waiting times at mn are going to increase.

## Slurm Logs

Slurm logs are stored within the `.snakemake/` directory created by 
Snakemake. You can access them with 

```
$ find .snakemake/slurm_logs/rule_test_rule/ -maxdepth 1
```

to see rule-level log directories.

## Issues & Future Additions

In the future, I would like to add some bash profile helper commands, e.g. for
quickly looking at the last log files, etc. If you want to work on adding these 
features, please create a new issue for discussion!

